<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Firebase\JWT\JWT;

class LoginController extends Controller
{
    private $key = "Ati\$a-token";

    /**
     * Comprueba si el login es válido para los datos mandados por POST
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function checkLogin(Request $request)
    {
        $data = json_decode($request->getContent());
        // En caso de que falte información devolvemos error
        if((!isset($data->email)) || (!isset($data->password)))
        {
            return $this->json(['message' => 'Introduzca su usuario y contraseña '. $request->getContent()], 401);
        }

        $exists = $this->verifyUser($data->email, $data->password);

        if(!$exists)
        {
            return $this->json(['message' => 'Usuario o contraseña incorrectos'], 401);
        }
        else
        {
            $token = $this->getToken($exists);            
            return $this->json([
            'message' => 'Login succesful!',
            'token' => $token
            ]);
        }

    }

    /**
     * Genera el token para utilizar en el front
     * @param $user
     * @return string
     */
    private function getToken($user)
    {
        $token = array(            
            "sub" => $user->getId(),
            "email" => $user->getEmail(),
            "iat" => time(),
            "exp" => time() + (7*24*60*60)
        );

        $jwt = JWT::encode($token, $this->key, 'HS256');
        return $jwt;
    }

    /**
     * Función que se utilizaría en un futuro para comprobar que el token es válido en las peticiones sucesivas
     * @param $token
     * @return bool
     */
    private function checkToken($token)
    {
        try
        {
            $decoded = JWT::decode($token, $this->key, array('HS256'));
            return true;

        }catch(\Exception $e)
        {
            return false;
        }
    }

    /**
     * Comprobamos que el usuario exista en la BBDD y que su contraseña sea correcta
     * @param $user
     * @param $pass
     * @return int|object|null
     */
    private function verifyUser($user,$pass)
    {
        $em = $this->getDoctrine()->getRepository(User::class);
        $exists = $em->findOneBy([
            'email' => $user,
            ]);
        if($exists)
        {
            $verify = password_verify($pass,$exists->getPassword());
            if($verify)   return $exists;
        }
        else
        {
            return 0;
        }
    }
}