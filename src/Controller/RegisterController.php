<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Firebase\JWT\JWT;

class RegisterController extends Controller
{
    public function wave ()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/RegisterController.php',
        ]);
    }

    public function register(Request $request)
    {
        $data = json_decode($request->getContent());
        //En caso de que falte algún dato devolvemos error
        if((!isset($data->email)) || (!isset($data->password))|| (!isset($data->name)))
        {
            return $this->json(['message' => 'Introduzca todos los datos'], 401);
        }

        $exists = $this->exists($data->email);
        if($exists)
        {
            return $this->json(['message' => 'El usuario ya existe en el sistema '], 409);
        }
        else
        {
            $this->registerUser($data);
            return $this->json([
                'message' => '¡Usuario creado!',
            ]);
        }

    }


    private function exists($user)
    {
        $em = $this->getDoctrine()->getRepository(User::class);
        $exists = $em->findOneBy([
            'email' => $user,
        ]);
        return $exists;
    }

    private function registerUser ($userData)
    {
        $createdAt = new \DateTime("now");
        $user = new User();
        $user->setEmail($userData->email);
        $user->setName($userData->name);
        $password = password_hash($userData->password, PASSWORD_DEFAULT);
        $user->setPassword($password);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
    }
}